import React from 'react'
import { ColorContext } from './ColorContext';

const WelcomeText = props => {
    const { Consumer } = ColorContext;

    return (
        <Consumer>
            {color => <p style={{ color }}>Hello World!</p>}
        </Consumer>
    )
}

export default WelcomeText;